import React, { Component } from 'react';
import './App.css'
import {TodoForm, TodoList} from './Components/Todo/'
import {addTodo, generateId} from './lib/TodoHelpers'
class App extends Component {

  state = {
    todos:[
      {id:1, name: 'jquery', isComplete: false},
      {id:2, name: 'angular', isComplete: true},
      {id:3, name: 'react', isComplete: false},
      {id:4, name: 'vue.js', isComplete: true}
    ],
    currentTodo: ''      
  }  

 

  handleInputChange = e => {
    this.setState({
      currentTodo: e.target.value
    })
  }

  handleOnSubmit = e => {
    e.preventDefault()
    const newId = generateId()
    const newTodo = {id:newId, name:this.state.currentTodo, isComplete:false}
    const updatedTodos = addTodo(this.state.todos , newTodo)
    this.setState({
      todos: updatedTodos,
      currentTodo: '',
      errorMessage: ''
    })
  }

  handleEmpytSubmit = e => {
    e.preventDefault()
    this.setState({
      errorMessage: 'need to add a todo'
    })
  }

  render() {
    const handleSubmit = this.state.currentTodo ? this.handleOnSubmit : this.handleEmpytSubmit
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Ract example</h1>
        </header>
        <div>
          <h2>List of todos</h2>
          {this.state.errorMessage && <span className="error">{this.state.errorMessage}</span>}
          <TodoForm
            currentTodo={this.state.currentTodo}
            handleInputChange={this.handleInputChange}
            handleOnSubmit={handleSubmit}
          />
          <TodoList todos={this.state.todos} />
        </div>
        
      </div>  
    );
  }
}

export default App;

import React from 'react'
import PropTypes from 'prop-types'

export const TodoForm = props => (
  <form onSubmit={props.handleOnSubmit}>
    <input
      type="text"
      name="currentTodo"
      value={props.currentTodo}
      onChange={props.handleInputChange}
        />
      <button>Save</button>
  </form>
)
//  Valida las propiedades 
// de este componente
TodoForm.propTypes = {
  currentTodo: PropTypes.string.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  handleOnSubmit: PropTypes.func.isRequired
}
import React  from 'react'
import PropTypes from 'prop-types'

export const TodoItem = props => {
  return (
    <li id={`li-todo-${props.id}`}>
        <input 
          type="checkbox"
          id={`li-input-${props.id}`}
          defaultChecked={props.isComplete}
          /> { props.name } 
     </li>
  )
}

TodoItem.propTypes = {
  id: PropTypes.number.isRequired,
  isComplete: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired
}
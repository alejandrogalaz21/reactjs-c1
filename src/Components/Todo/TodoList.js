import React from 'react'
import PropTypes from 'prop-types'
import {TodoItem} from './TodoItem'


export const TodoList = props => (
  <ul>
    {
    props.todos.map(t => <TodoItem key={t.id} {...t} /> )
    }
  </ul>
)

TodoList.propTypes = {
  todos: PropTypes.array.isRequired
}

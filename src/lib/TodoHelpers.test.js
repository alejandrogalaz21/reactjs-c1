import {addTodo, findById, toggleTodo} from './TodoHelpers'

test('add todo should add the passed todo to the list', () => {
  const startTodos = [
    {id:1, name: 'jquery', isComplete: false},
    {id:2, name: 'angular', isComplete: true}
  ]

  const newTodo = {id:3, name: 'react', isComplete: false}
  
  const resultE = [
    {id:1, name: 'jquery', isComplete: false},
    {id:2, name: 'angular', isComplete: true},
    {id:3, name: 'react', isComplete: false}
  ]

  const result = addTodo(startTodos, newTodo)

  expect(result).toEqual(resultE)
})

test('findById should return the expected item from an array', () => {
  const todos = [
    {id:1, name: 'jquery', isComplete: false},
    {id:2, name: 'angular', isComplete: true}
  ]

  const expected = {id:2, name: 'angular', isComplete: true}
  
  const result = findById(todos, 2)
  
  expect(result).toEqual(expected)
})

test('toggleTodo should toggle the isComplete prop of a todo', () => {

  const todo = {id:2, name: 'angular', isComplete: false}
  
  const expected = {id:2, name: 'angular', isComplete: true}
  
  const result = toggleTodo(todo)
  
  expect(result).toEqual(expected)
})

test('toggleTodo should not mutate the original todo', () => {
  
  const todo = {id:2, name: 'angular', isComplete: false}
  
  const result = toggleTodo(todo)
  
  expect(result).not.toBe(todo)
})